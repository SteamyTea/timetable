import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader version "^2.1.1" ,
import minifyTheme from 'minify-css-string'
import LRU from 'lru-cache'

Vue.use(Vuetify)


const themeCache = new LRU({
  max: 10,
  maxAge: 1000 * 60 * 60, // 1 hour
})

export default ctx => {
  const vuetify = new Vuetify({
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: '#bb86fc',
          accent: '#37474F',
          secondary: '#03DAC5',
          info: '#03da9d',
          warning: '#cfbe66',
          error: '#CF6679',
          success: '#03DAC5',
          font: '#919191',
        },
      },
      options: {
        minifyTheme,
        themeCache,
      },
    }
  })

  ctx.app.vuetify = vuetify
  ctx.$vuetify = vuetify.framework
}
