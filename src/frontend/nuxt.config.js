export default {
  server: {
    host: "0.0.0.0",
    port: process.env.PORT,
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s - frontend',
    title: 'frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  /**
   * Google is preferring images that are cached longer
   */
  render: {
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
    },
  },

  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '@/plugins/axios.js', mode: 'client' },
    { src: '@/plugins/vue-chartjs.js', mode: 'client' },
    { src: '@/plugins/vuetify.js' },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', { fix: true }],
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'nuxt-i18n',
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  /*
   ** i18n module configuration
   ** https://nuxt-community.github.io/nuxt-i18n/basic-usage.html
   */
  i18n: {
    seo: false,
    lazy: true,
    defaultLocale: 'de',
    strategy: 'prefix_except_default',
    langDir: 'lang/',
    locales: [
      {
        code: 'de',
        iso: 'de-DE',
        name: 'Deutsch',
        file: 'de-DE.js',
      },
      {
        code: 'en',
        iso: 'en-US',
        name: 'English',
        file: 'en-US.js',
      },
    ],
    parsePages: true, // Babel parsing
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
