export default {
  general: {
    projectName: 'Timetable',
    baseUrl: 'studentproject.me',
  },
  landing: {
    title: {
      firstLine: 'time management',
      secondLine: 'done right',
      thirdLine: 'Fast scheduling and management of working hours.'
    }
  },
  login: {
    title: "Logge Dich in Deinen Account ein",
    email: "E-Mail",
    emailError: {
      valid: "Muss eine valide E-Mail sein",
      required: "E-Mail wird benötigt"
    },
    passwordError: {
      minimum: "Muss mindestens 6 Zeichen enthalten",
      maximum: "Darf maximal 50 Zeichen enthalten",
      required: "Passwort wird benötigt"
    },
    password: "Passwort",
    forgotPassword: "Passwort vergessen?",
    login: "Weiter",
    register: {
      question: "Du hast noch keinen Account?",
      linkText: "registrieren"
    },
    staySignedIn: "Bleib für eine Woche eingeloggt"
  },
  register: {
    title: "Erstelle deinen Timetable account",
    email: "E-Mail",
    emailError: {
      valid: "Muss eine valide E-Mail sein",
      required: "E-Mail wird benötigt"
    },
    passwordError: {
      minimum: "Mindestens 6 Zeichen",
      maximum: "Maximal 50 Zeichen",
      required: "Passwort wird benötigt"
    },
    password: "Passwort",
    name: "Full name",
    nameErrors: {
      minimum: "Mindestens 1 Zeichen",
      maximum: "Maximal 1500 Zeichen",
      alpha: "Ein name kann nur aus Buchstaben bestehen",
      required: "Name wird benötigt"
    },
    username: "Benutzername",
    usernameErrors: {
      minimum: "Mindestens 4 Zeichen",
      maximum: "Maximal 150 Zeichen",
      required: "Benutzername wird benötigt"
    },
    register: "Erstelle einen Account",
    login: {
      question: "Du hast bereits einen Account?",
      linkText: "Logge Dich ein"
    },
    productUpdates: {
      description: "Sende mir keine Updates über das Produkt. Wenn diese Box ausgelassen wird, sendet dir Timetable gelegentlich hilfreiche E-Mails über Produktupdates. Du kannst dies jederzeit im Profil ändern.",
      linkText: "Privacy Policy"
    }
  },
  reset: {
    description: "Gib die E-Mail Addresse, die mit deinem Account verbunden ist ein und wir senden Dir einen Link, um dein Passwort zurückzusetzen.",
    email: "E-Mail",
    reset: "Weiter",
    return: "Zurück zum login",
  },
  contact: {
    name: {
      label: 'Name',
      error: {
        minLength: 'Mindestens Länge von 1 Buchstaben',
        maxLength: 'Maximale Länge von 10000 Buchstaben',
        required: 'Name ist notwenig',
      },
    },
    email: {
      label: 'Email',
      error: {
        email: 'Muss eine valide Email sein.',
        required: 'Email ist notwenig.',
      },
    },
    textField: {
      label: 'Nachricht',
      error: {
        minLength: 'Mindestens Länge von 1 Buchstaben',
        maxLength: 'Maximale Länge von 10000 Buchstaben',
        required: 'Text ist notwenig',
      },
    },
  },
  error: {
    404: {
      title: 'Irgendwas fehlt',
      body: 'Die Seite fehlt oder du hast den Link falsch zusammengebaut.',
    },
    500: {
      title: 'Oh, es ist ein Fehler aufgetreten.',
      body: 'Ich wurde darüber benachrichtigt und kümmere mich drum.',
    },
    return: 'Zurück',
  },
  navbar: {
    login: 'Anmelden',
  },
  footer: {
    companyDetails: 'Impressum',
    contact: 'Kontakt',
    dataProtection: 'Datenschutz',
  },
}
