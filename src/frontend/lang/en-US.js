export default {
  general: {
    projectName: 'Timetable',
    baseUrl: 'studentproject.me',
  },
  landing: {
    title: {
      firstLine: 'time management',
      secondLine: 'done right',
      thirdLine: 'Fast scheduling and management of working hours.'
    }
  },
  login: {
    title: "Sign in to your account",
    email: "Email",
    emailError: {
      valid: "Must be a valid email",
      required: "Email is required"
    },
    passwordError: {
      minimum: "Minimum length of 6 characters",
      maximum: "Maximum length of 50 characters",
      required: "Password is required"
    },
    password: "Password",
    forgotPassword: "Forgot your password?",
    login: "Continue",
    register: {
      question: "Don't have an account?",
      linkText: "Sign up"
    },
    staySignedIn: "Stay signed in for a week"
  },
  register: {
    title: "Create your Timetable account",
    email: "Email",
    emailError: {
      valid: "Must be a valid email",
      required: "Email is required"
    },
    passwordError: {
      minimum: "Minimum length of 6 characters",
      maximum: "Maximum length of 50 characters",
      required: "Password is required"
    },
    password: "Password",
    firstname: "Firstname",
    firstnameErrors: {
      minimum: "Minimum length of 1 characters",
      maximum: "Maximum length of 1500 characters",
      alpha: "A name can only consist of letters",
      required: "Firstname is required"
    },
    surname: "Surname",
    surnameErrors: {
      minimum: "Minimum length of 1 characters",
      maximum: "Maximum length of 1500 characters",
      alpha: "A name can only consist of letters",
      required: "Surname is required"
    },
    username: "Username",
    usernameErrors: {
      minimum: "Minimum length of 4 characters",
      maximum: "Maximum length of 150 characters",
      required: "Username is required"
    },
    register: "Create an account",
    login: {
      question: "Have and account?",
      linkText: "Sign in"
    },
    productUpdates: {
      description: "Don't email me about product updates. If this box is left unchecked, Timetable will occasionally send helpful and relevant emails. You can unsubscribe at any time.",
      linkText: "Privacy Policy"
    }
  },
  reset: {
    description: "Enter the email address associated with your account and we'll send you a link to reset your password.",
    email: "Email",
    reset: "Continue",
    return: "Return to sign in",
  },
  contact: {
    name: {
      label: 'Name',
      error: {
        minLength: 'Minimum length of 1 character',
        maxLength: 'Maximum length of 10000 characters',
        required: 'Name field is required.',
      },
    },
    email: {
      label: 'Email',
      error: {
        email: 'Must be a valid email.',
        required: 'Email is required.',
      },
    },
    textField: {
      label: 'Message',
      error: {
        minLength: 'Minimum length of 1 character',
        maxLength: 'Maximum length of 10000 characters',
        required: 'Text is required.',
      },
    },
  },
  error: {
    404: {
      title: 'Something went missing',
      body: 'The page is missing or you assembled the link incorrectly',
    },
    500: {
      title: 'Oh, no. An error occurred',
      body: 'I was notified and am working on the issue.',
    },
    return: 'Go Back',
  },
  navbar: {
    login: 'Sign in',
  },
  footer: {
    companyDetails: 'Company Details',
    contact: 'Contact',
    dataProtection: 'Data Protection',
  },
}
