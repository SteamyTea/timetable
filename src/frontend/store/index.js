import feedback from './modules/feedback'

const state = () => ({})

const modules = {
  feedback,
}

export default {
  namespaced: true,
  strict: false,
  modules,
  state,
}
