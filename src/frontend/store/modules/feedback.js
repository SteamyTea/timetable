const state = () => ({
  message: null,
})

const mutations = {
  setMessage(state, message) {
    state.message = message
  },
}

const getters = {
  message(state) {
    return state.message
  },
}

export default {
  name: 'feedback',
  namespaced: true,
  mutations,
  state,
  getters,
}
