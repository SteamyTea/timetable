"use strict"

const Pkg = require("../package")

const Consola = require("consola")
const Boom = require("@hapi/boom")
const Hapi = require("@hapi/hapi")
const Path = require("path")
const Crypto = require("crypto")

// Application will fail if environment variables are not set
if(!process.env.NODE_ENV) {
  const errMsg = "NODE_ENV environment variable is not defined"
  console.error(errMsg)
  throw new Error(errMsg)
}

if(!process.env.PORT) {
  const errMsg = "PORT environment variable is not defined"
  console.error(errMsg)
  throw new Error(errMsg)
}

if(!process.env.DB_ADDR) {
  const errMsg = "DB_ADDR environment variable is not defined"
  console.error(errMsg)
  throw new Error(errMsg)
}

if(!process.env.DB_NAME) {
  const errMsg = "DB_NAME environment variable is not defined"
  console.error(errMsg)
  throw new Error(errMsg)
}

if(!process.env.SALT_ROUNDS) {
  const errMsg = "SALT_ROUNDS environment variable is not defined"
  console.error(errMsg)
  throw new Error(errMsg)
}

// Defining variables
const environment = process.env.NODE_ENV
const serverHost = "0.0.0.0"
const serverPort = process.env.PORT
const baseUrl = process.env.BASE_URL || serverHost + ":" + serverPort

const dbUri = process.env.DB_ADDR
const dbName= process.env.DB_NAME

const saltRounds = parseInt(process.env.SALT_ROUNDS)

const mailerHost = process.env.MAILER_HOST || "localhost"
const mailerPort = process.env.MAILER_PORT || "4011"

// =============================
// Define Plugins
// =============================
const plugRoutes = {
  plugin: require("hapi-router"),
  options: {
    routes: [
      "./server/routes/favicon.ico",
      "./server/routes/index.route.js",
      "./server/routes/*/**/**/**/**/**/**/**/*.route.*",
    ],
  },
}

const plugMongo = {
  plugin: require("hapi-multi-mongo"),
  options: {
    connection: {
      uri: dbUri,
      name: dbName,
    },
    options: {
      native_parser: false,
      useUnifiedTopology: true,
    },
  },
}

const plugPino = {
  plugin: require("hapi-pino"),
  options: {
    prettyPrint: environment !== 'production',
    redact: ['req.headers.authorization']
  },
}

const plugSwagger = {
  plugin: require("hapi-swagger"),
  options: {
    info: {
      title: "API Documentation",
      version: Pkg.version,
    },
    routeTag: "api",
    grouping: "tags",
    payloadType: "json",
    basePath: "/api/",
    documentationPath: "/documentation",
    schemes: ["https"],
    cors: true,
    swaggerUI: true,
    documentationPage: true,
    reuseDefinitions: false,
    securityDefinitions: {
      apiKey: {
        type: "apiKey",
        name: "authorization",
        in: "header",
      },
    },
  },
}

// Initialize a new server object
export const init = async () => {
  // Create server object
  const server = new Hapi.Server({
    host: serverHost,
    port: serverPort,
    routes: {
      payload: {
        maxBytes: 50000000,
      },
      cors: {
        origin: ["*"],
        credentials: true,
      },
      security: {
        hsts: {
          maxAge: 15552000,
          includeSubdomains: true,
        },
        xframe: true,
        xss: true,
        noOpen: false,
        noSniff: true,
      },
      files: {
        relativeTo: Path.join(__dirname, "public"),
      },
      validate: {
        failAction: (request, h, err) => {
          if (environment === "production") {
            // In prod, log a limited error message and throw the default Bad Request error.
            console.error("ValidationError:", err.message)
            throw Boom.badRequest(`Invalid request payload input`)
          } else {
            // During development, log and respond with the full error.
            console.error(err)
            throw err
          }
        },
      },
    },
  })

  process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = "";

  // Defining server variables
  server.app.robots =
    process.env.NODE_ENV === "production"
      ? "noindex, nofollow"
      : "noindex, nofollow"

  server.app.baseUrl = baseUrl
  server.app.serverHost = serverHost
  server.app.serverPort = serverPort

  server.app.dbUri = dbUri
  server.app.dbName = dbName

  server.app.saltRounds = saltRounds

  server.app.jwtAccessTokenSecret = 1 === 1 ? "TEST" : Crypto.randomBytes(256).toString('base64');
  server.app.jwtRefreshTokenSecret = 1 === 1 ? "TEST" : Crypto.randomBytes(256).toString('base64')
  server.app.jwtAccessTokenTime = "15m"
  server.app.jwtRefreshTokenTime = "14d"

  server.app.mailerHost = mailerHost
  server.app.mailerPort = mailerPort

  // TODO Auth Strategy
  await server.register([require("hapi-auth-jwt2")])
  server.auth.strategy('jwt', 'jwt',
      { key: server.app.jwtAccessTokenSecret, // Never Share your secret key
        validate,  // validate function defined below
      });

  server.auth.default('jwt');

  // Register plugins
  await server.register([require("@hapi/inert")])
  console.log("Server registered @hapi/inert successfully")

  await server.register([require("@hapi/vision")])
  console.log("Server registered @hapi/vision successfully")

  await server.register(plugSwagger);
  console.log("Server registered swagger successfully")

  await server.register(plugPino)
  console.log("Server registered laabr successfully")

  await server.register(plugMongo)
  console.log("Server registered mongodb successfully")

  await server.register(plugRoutes)
  console.log("Server registered routes successfully")

  await server.initialize()
  console.log("Server initialized successfully")

  return server
}

export const start = async () => {
  const serverInstance = await exports.init()

  Consola.info("baseurl: " + serverInstance.app.baseUrl)
  Consola.info("port: " + serverInstance.info.port)

  await serverInstance.start()

  Consola.ready({
    message: `Server running at: ${serverInstance.info.uri}`,
    badge: true,
  })

  return serverInstance
}


process.on("unhandledRejection", (error) => Consola.error(error))

const validate = async (decoded: any) => {
  if (decoded && decoded.sub) {
    return {
      isValid: true,
      credentials: decoded
    }
  }

  return { isValid: false };
};
