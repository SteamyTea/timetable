"use strict"

const route = {
  path: "/",
  method: "GET",
  handler: (req, h) => {
    return "Welcome to the TimeTable API"
  },
  options: {
    description: "Default landing page. Used for testing the connection.",
    notes: "Returns the static index.html",
    tags: ["api", "testing"],
    auth: false,
    plugins: {
      "hapi-swagger": {
        responses: {
          200: {
            description: "Successfully retrieved the index page",
          },
        },
      },
    },
  }
}
export = [route]