"use strict"

const Joi = require("joi")
import { refresh } from "../../controller/v1/auth.controller"

const route = {
    path: "/api/v1/auth/refresh",
    method: "POST",
    handler: refresh,
    options: {
        auth: false,
        description: "Verifies the refresh token and returns an access token.",
        notes: "Returns an access token",
        tags: ["api", "user",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully created a new user object",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated user does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    refresh_token: Joi.string().max(2000).required().description("Refresh token"),
                }
            ).label("Refresh")
        }
    }
}

export = route