"use strict"

const Joi = require("joi")
import { passwordReset } from "../../controller/v1/auth.controller"

const route = {
    path: "/api/v1/auth/forgotPassword",
    method: "POST",
    handler: passwordReset,
    options: {
        auth: false,
        description: "Verifies that an account with that email exists and sends an reset email to the user",
        notes: "Send an email",
        tags: ["api", "user",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    200: {
                        description: "Successfully send the reset email",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated user does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    email: Joi.string().email({ tlds: { allow: false } }).required().description("Email address"),
                }
            ).label("Login")
        }
    }
}

export = route