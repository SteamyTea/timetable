"use strict"

const Joi = require("joi")
import { getUserFromAuthorizationToken } from "../../controller/v1/auth.controller"

const route = {
    path: "/api/v1/auth/user",
    method: "GET",
    handler: getUserFromAuthorizationToken,
    options: {
        auth: {
            access: [{         // you can configure different access for each route
                scope: ['User', 'Reviewer', 'Admin'] // each access can define a set of scopes with type prefix ('+','-' or empty)
            }]
        },
        description: "Retrieves the user with _id={id}.",
        notes: "Checks if the user exists and either return the user or a 404",
        tags: ["api", "user",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully retrieved the user object",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated user does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
    }
}

export = route