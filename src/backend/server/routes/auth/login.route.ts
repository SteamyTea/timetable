"use strict"

const Joi = require("joi")
import { login } from "../../controller/v1/auth.controller"

const route = {
    path: "/api/v1/auth/login",
    method: "POST",
    handler: login,
    options: {
        auth: false,
        description: "Verifies the email and password.",
        notes: "Returns a refresh token",
        tags: ["api", "user",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully authenticated",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated user does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    email: Joi.string().email({ tlds: { allow: false } }).required().description("Email address"),
                    password: Joi.string().min(6).max(200).required().description("Password"),
                }
            ).label("Login")
        }
    }
}

export = route