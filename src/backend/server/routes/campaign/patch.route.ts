"use strict"

const Joi = require("joi")
import { partiallyUpdateOneById } from "../../controller/v1/campaign.controller"

const route = {
    path: "/api/v1/campaigns/{id}",
    method: "PATCH",
    handler: partiallyUpdateOneById,
    options: {
        auth: {
            access: [{         // you can configure different access for each route
                scope: ['Admin', 'Reviewer'] // each access can define a set of scopes with type prefix ('+','-' or empty)
            }]
        },
        description: "Partially updates a campaign.",
        notes: "Returns the updated campaign object",
        tags: ["api", "campaign",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully updated the campaign object",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated campaign does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    title: Joi.string().allow("", null).max(200).optional(),
                    description: Joi.string().allow("", null).max(3200).optional(),
                    startDate: Joi.string().allow("", null).max(3200).optional(),
                    endDate: Joi.string().allow("", null).max(3200).optional(),
                    status: Joi.string().allow("", null).valid('Pending','In Review', 'Approved', 'Declined').max(200).optional(),
                    tags: Joi.array().items(Joi.string().allow("", null).max(200).optional()),
                    socialMedia: Joi.object({
                        instagram: Joi.string().allow("", null).max(3200).optional(),
                        facebook: Joi.string().allow("", null).max(3200).optional(),
                        twitter: Joi.string().allow("", null).max(3200).optional(),
                        xing: Joi.string().allow("", null).max(3200).optional(),
                        linkedIn: Joi.string().allow("", null).max(3200).optional(),
                        website: Joi.string().allow("", null).max(3200).optional(),
                    })
                }
            ).label("UpdateCampaign")
        }
    }
}

export = route