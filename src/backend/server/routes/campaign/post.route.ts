"use strict"

const Joi = require("joi")
import { createOne } from "../../controller/v1/campaign.controller"

const route = {
    path: "/api/v1/campaigns",
    method: "POST",
    handler: createOne,
    options: {
        auth: {
            access: [{         // you can configure different access for each route
                scope: ['User', 'Reviewer', 'Admin'] // each access can define a set of scopes with type prefix ('+','-' or empty)
            }]
        },
        description: "Creates a new campaign.",
        notes: "Returns an auth token",
        tags: ["api", "campaign",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully created a new campaign object",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated campaign does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    title: Joi.string().max(200).required(),
                    description: Joi.string().max(3200).required(),
                    tags: Joi.array().items(Joi.string().required().max(200))
                }
            ).label("Createcampaign")
        }
    }
}

export = route