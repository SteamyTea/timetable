'use strict';

import { get } from "../../controller/v1/campaign.controller"

const Joi = require("joi")

const route = {
  path: '/api/v1/campaigns',
  method: 'GET',
  handler: get,
  options: {
    // auth: {
    //   access: [{         // you can configure different access for each route
    //     scope: ['User', 'Reviewer', 'Admin'] // each access can define a set of scopes with type prefix ('+','-' or empty)
    //   }]
    // },
    description: 'Retrieves a list of  the user can choose from',
    notes:
      'Returns a list of campaigns. By default the campaigns are returned sorted alphabetically by name and a maximum of 10 entries is returned.',
    tags: ['api', 'theme'],
    plugins: {
      'hapi-swagger': {
        security: ['apiKey'],
        responses: {
          200: {
            description:
              'Successfully retrieved a list of campaigns. Returns an empty array if no value was found.',
          },
          400: {
            description: 'Validation failed.',
          },
          401: {
            description:
              'No authentication credentials provided or authentication failed',
          },
          403: {
            description: 'Authenticated user does not have access',
          },
          429: {
            description: 'Request rejected due to rate limiting',
          },
          500: {
            description: 'An internal server error occurred',
          },
        },
      },
    },
    validate: {
      query: Joi.object({
        limit: Joi.number()
          .integer()
          .min(1)
          .max(100)
          .optional()
          .description(
            'A limit on the number of objects to be returned. Limit can range between 1 and 100, and the default is 10.'
          ),
        skip: Joi.number()
          .integer()
          .min(0)
          .optional()
          .description(
            'An offset on the returned result set. Starting from 1.'
          ),
        fields: Joi.string()
          .allow('')
          .optional()
          .description(
            'A list of object fields that should be returned. The fields are comma separated. The _id field is accessible as id. Example: fields=description,id'
          ),
        sort: Joi.string()
          .allow('')
          .optional()
          .description(
            'The list is sorted by the list of fields, which are separated by comma. The order of the fields determines the sort order. Available sort fields are: name'
          ),
        count: Joi.boolean()
          .optional()
          .description(
            'All endpoints that return a collection can provide a count of the total number of results. To request a count, just include a count=true as a query parameter. The count will be returned in a header Total-Count'
          ),
        id: Joi.string()
          .allow('')
          .optional()
          .description(
            'A filter on the list based on the object _id field. The value can be a string or an ObjectID.'
          ),
      }),
    },
  },
};

export = route;
