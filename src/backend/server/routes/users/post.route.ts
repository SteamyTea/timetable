"use strict"

const Joi = require("joi")
import { createOne } from "../../controller/v1/user.controller"

const route = {
    path: "/api/v1/users",
    method: "POST",
    handler: createOne,
    options: {
        auth: false,
        description: "Creates a new user.",
        notes: "Returns an auth token",
        tags: ["api", "user",],
        plugins: {
            "hapi-swagger": {
                security: ["apiKey"],
                responses: {
                    201: {
                        description: "Successfully created a new user object",
                    },
                    400: {
                        description: "Validation failed."
                    },
                    401: {
                        description: "No authentication credentials provided or authentication failed"
                    },
                    403: {
                        description: "Authenticated user does not have access"
                    },
                    405: {
                        description: "POST/PUT/PATCH request occurred without a application/json content type"
                    },
                    429: {
                        description: "Request rejected due to rate limiting"
                    },
                    500: {
                        description: "An internal server error occurred"
                    }
                }
            }
        },
        validate: {
            payload: Joi.object(
                {
                    firstName: Joi.string().max(200).required().description("First name, given name"),
                    lastName: Joi.string().max(200).required().description("Last name, family name"),
                    email: Joi.string().email({ tlds: { allow: false } }).required().description("Email address"),
                    password: Joi.string().min(6).max(200).required().description("Password"),
                }
            ).label("CreateUser")
        }
    }
}

export = route