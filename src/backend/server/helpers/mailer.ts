'use strict'

const Fs = require('fs')
const Path = require('path')
const Boom = require('boom')
const Util = require('util')
const Nodemailer = require('nodemailer')
const Handlebars = require('handlebars')
const htmlToText = require('html-to-text')
const ReadFile = Util.promisify(Fs.readFile)
const PostmarkTransport = require('nodemailer-postmark-transport')
const Transporter = Nodemailer.createTransport({
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT
})

const Templates = Path.resolve(__dirname, '..', 'data/emailTemplates/eng')

/**
 * filename: email template name, without ".html" file ending. Email templates are located within "server/email-templates"
 * options: data which will be used to replace the placeholders within the template
 **/
async function prepareTemplate (filename, options = {}) {
    try {
        const templatePath = Path.resolve(Templates, `${filename}.html`)
        const content = await ReadFile(templatePath, 'utf8')

        // use handlebars to render the email template
        // handlebars allows more complex templates with conditionals and nested objects, etc.
        // this way we have much more options to customize the templates based on given data
        const template = Handlebars.compile(content)
        const html = template(options)

        // generate a plain-text version of the same email
        const text = htmlToText.fromString(html)

        return {
            html,
            text
        }
    } catch (error) {
        throw new Boom('Cannot read the email template content.')
    }
}

export const send = async (template, email, subject, data) => {
    // @ts-ignore
    const { html, text } = await prepareTemplate(template, data)
    const mailOptions = {
        from: `DFB <noreply@dfb.com>`,
        to: email,
        subject: subject,
        html,
        text
    }

    try {
        await Transporter.sendMail(mailOptions)
    } catch (err) {
        console.log(err)
    }
}