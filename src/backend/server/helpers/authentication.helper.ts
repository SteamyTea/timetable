"use strict"

const Jwt = require("jsonwebtoken")
const Bcrypt = require("bcrypt")

export const createToken = async (payload, subject, key, expiresIn: String = "24h") : Promise<String> => {
  const token : String = await new Promise((resolve, reject) => {
    Jwt.sign(
      payload,
      key,
      {
        algorithm: "HS256",
        expiresIn: expiresIn,
        issuer: "https://localhost",
        subject: subject,
      },
      function (err, data) {
        if (err) reject(err)
        resolve(data)
      }
    )
  })

  return token
}

export const hashPassword = async (password, saltRounds) : Promise<String> => {
  const hashedPassword : String = await new Promise((resolve, reject) => {
    Bcrypt.hash(password, saltRounds, function (err, hash) {
      if (err) reject(err)
      resolve(hash)
    })
  })

  return hashedPassword
}
