export const getDatabase = async (server) => {
    const mongos = await server.plugins["hapi-multi-mongo"].mongo

    // TODO implement logic to fetch the right database

    return mongos[server.app.dbName].db()
}

export const getClient = async (server) =>  {
    const mongos = await server.plugins["hapi-multi-mongo"].mongo

    // TODO implement logic to fetch the right database

    return mongos[server.app.dbName]
}