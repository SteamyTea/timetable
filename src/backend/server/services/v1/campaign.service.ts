"use strict";

import { Models } from "../../types/models";
import Campaign = Models.DFB.Campaign;

const Collections = require("../../data/collections")
const moment = require('moment');
const ObjectId = require("mongodb").ObjectID;


export const getMany = async (db, searchQuery: Object, limit: Number, skip: Number, sort: Object, projection: Object) => {
    const clientCollection = await db.collection(Collections.v1.campaign)

    try {
        return clientCollection
            .aggregate([
                { $match: searchQuery },
                { $skip: skip },
                { $limit: limit },
                { $sort: sort },
                { $project: projection }
            ])
            .toArray()
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getCount = async (db, searchQuery: Object) => {
    const clientCollection = await db.collection(Collections.v1.campaign)

    try {
        return clientCollection.find(searchQuery).count()
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const insertOne = async (db, campaign: Campaign) => {
    const clientCollection = await db.collection(Collections.v1.campaign)

    try {
        const insertedCampaign = await clientCollection.insertOne(campaign);
        return insertedCampaign.ops[0];
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getOne = async (db, searchQuery: Object, projection: Object) => {
    const clientCollection = await db.collection(Collections.v1.campaign)

    try {
        return clientCollection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getUserOne = async (db, searchQuery: Object, projection: Object) => {
    const clientCollection = await db.collection(Collections.v1.users)

    try {
        return clientCollection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getStatusOne = async (db, searchQuery: Object, projection: Object) => {
    const clientCollection = await db.collection(Collections.v1.statuses)

    try {
        return clientCollection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

/**
 *
 * @param db
 * @param searchQuery
 * @param campaign
 * @return Returns the updated object, may return NULL if no object was found or if an error occurred
 */
export const updateOne = async (db: any, searchQuery: Object, campaign: Campaign) => {
    const clientCollection = await db.collection(Collections.v1.campaign)
    try {
        const updatedCampaign = await clientCollection.findOneAndUpdate(
            searchQuery,
            { $set: campaign },
            { returnOriginal: false }
        )
        return updatedCampaign
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}