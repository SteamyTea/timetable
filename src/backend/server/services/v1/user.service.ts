"use strict";

import {Models} from "../../types/models";
import User = Models.DFB.User;


const Collections = require("../../data/collections.json")
const moment = require('moment');
const ObjectId = require("mongodb").ObjectID;


export const getMany = async (db, searchQuery: Object, limit: Number, skip: Number, sort: Object, projection: Object) : Promise<Array<User>> => {
    const userCollection = await db.collection(Collections.v1.users)

    try {
        return userCollection
            .aggregate([
                { $match: searchQuery },
                { $skip: skip },
                { $limit: limit },
                { $sort: sort },
                { $project: projection }
            ])
            .toArray()
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getCount = async (db, searchQuery: Object): Promise<User> => {
    const userCollection = await db.collection(Collections.v1.users)

    try {
        return userCollection.find(searchQuery).count()
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const insertOne = async (db: any, user: User) => {
    const userCollection = await db.collection(Collections.v1.users)

    try {
        const insertedUser = await userCollection.insertOne(user);
        return insertedUser.ops[0];
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getOne = async (db: any, searchQuery: Object, projection: Object): Promise<User> => {
    const userCollection = await db.collection(Collections.v1.users)

    try {
        return userCollection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getUserOne = async (db: any, searchQuery: Object, projection: Object) : Promise<User> => {
    const userCollection = await db.collection(Collections.v1.users)

    try {
        return userCollection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

/**
 *
 * @param db
 * @param searchQuery
 * @param User
 * @return Returns the updated object, may return NULL if no object was found or if an error occurred
 */
export const updateOne = async (db: any, searchQuery: Object, User: User): Promise<User> => {
    const userCollection = await db.collection(Collections.v1.users)
    try {
        const updatedUser= await userCollection.findOneAndUpdate(
            searchQuery,
            { $set: User },
            { returnOriginal: false }
        )
        return updatedUser
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}