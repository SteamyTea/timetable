"use strict";

import {Models} from "../../types/models";
import AuthAccount = Models.DFB.AuthAccount;
import Meta = Models.DFB.Meta;


const Collections = require("../../data/collections.json")
const moment = require('moment');
const ObjectId = require("mongodb").ObjectID;

export const insertOne = async (db: any, authAccount: AuthAccount): Promise<AuthAccount> => {
    const collection = await db.collection(Collections.v1.authAccounts)

    try {
        const insertedAuthAccount = await collection.insertOne(authAccount);
        return insertedAuthAccount.ops[0];
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

export const getOne = async (db: any, searchQuery: Object, projection: Object): Promise<AuthAccount> => {
    const collection = await db.collection(Collections.v1.authAccounts)

    try {
        return collection.findOne(searchQuery, projection)
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

/**
 *
 * @param db
 * @param searchQuery
 * @param authAccount
 * @return Returns the updated object, may return NULL if no object was found or if an error occurred
 */
export const updateOne = async (db: any, searchQuery: Object, authAccount: AuthAccount): Promise<AuthAccount> => {
    const collection = await db.collection(Collections.v1.authAccounts)
    try {
        const authAccountUpdated = await collection.findOneAndUpdate(
            searchQuery,
            {$set: authAccount},
            {returnOriginal: false}
        )
        return authAccountUpdated
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}

/**
 *
 * @param db
 * @param searchQuery
 * @param token
 * @param meta
 * @return Returns the updated object, may return NULL if no object was found or if an error occurred
 */
export const pushRefreshToken = async (db: any, searchQuery: Object, token: String, meta: Meta): Promise<AuthAccount> => {
    const collection = await db.collection(Collections.v1.authAccounts)
    try {
        const authAccountUpdated = await collection.update(
            searchQuery,
            {
                $push: {
                    refreshTokens: token
                },
                $set: {
                    meta: meta
                }
            },
            {returnOriginal: false}
        )
        return authAccountUpdated
    } catch (e) {
        console.log("The transaction was aborted due to an unexpected error: " + e)
        return null
    }
}