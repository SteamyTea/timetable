"use strict"

import { start } from "./index"

start().catch((error) => {
  console.log(error)
})
