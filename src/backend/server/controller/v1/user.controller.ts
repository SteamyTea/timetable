"use strict"

import * as Hapi from "@hapi/hapi";
const Roles = require("../../data/roles.json")
import * as UserService from "../../services/v1/user.service";
import * as AuthService from "../../services/v1/auth.service";
import { Models } from "../../types/models";
import { getDatabase } from "../../helpers/db.helper";
import { createToken, hashPassword } from "../../helpers/authentication.helper";
import {send} from "../../helpers/mailer";
const ObjectId = require("mongodb").ObjectID
const sanitize = require("mongo-sanitize");
const moment = require('moment');


const DEFAULT_SKIP = 0;
const DEFAULT_LIMIT = 10000;


/**
 *
 * @param fields
 */
const getProjection = (fields) => {
    let fieldsProjection = {}
    if (fields) {
        const fieldsKeyWords = fields.split(',');
        for (const fieldKeyWord of fieldsKeyWords) {
            fieldsProjection[fieldKeyWord] = 1
        }
    } else {
        fieldsProjection = { _id: 1, title: 1, description: 1, startDate: 1, endDate: 1, tags: 1, socialMedia: 1,user: 1, meta: 1}
    }

    return fieldsProjection
}

/**
 *
 * @param sort
 */
const getSortQuery = (sort) => {
    let sortQuery = {}
    if (sort) {
        const sortKeyWords = sort.split(',');
        for (const sortKeyWord of sortKeyWords) {
            const index = sortKeyWord.charAt(0) === "-" ? sortKeyWord.substring(1, sortKeyWord.length) : sortKeyWord
            sortQuery[index] = (sortKeyWord.charAt(0) === "-") ? -1 : 1
        }
    } else {
        sortQuery = {
            title: 1
        }
    }

    return sortQuery
}

/**
 *
 * @param search
 */
const getSearchQuery = (search) => {
    const { id, title } = search

    const searchQuery = {
        _id: ObjectId,
        title: String
    }

    if (id) {
        searchQuery._id = ObjectId(id)
    }
    if (name) {
        searchQuery.title = title
    }

    return searchQuery
}

/**
 * Retrieved one or multiple campaigns
 * @param req
 * @param h
 */
export const get = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {
    // User input
    const { limit, skip, fields, sort, ...searchQueryVariables } = req.query;

    // TODO Input sanitization

    // Database Query Construction
    const searchQuery = getSearchQuery(searchQueryVariables);
    const sortQuery = getSortQuery(sort);
    const projectionQuery = getProjection(fields);
    const limitQuery = DEFAULT_LIMIT;
    const skipQuery = DEFAULT_SKIP;

    // Database Connection
    const db = await getDatabase(req.server);
    const data = await UserService.getMany(db, searchQuery, limitQuery, skipQuery, sortQuery, projectionQuery);
    const dataCount = await UserService.getCount(db, searchQuery);

    // Pagination
    const pagination = { limit: limit, skip: skip, count: Array.isArray(data) ? data.length : 1, totalElements: dataCount }

    // Return values
    return h
        .response({ data, pagination })
        .code(200)
}

/**
 * Retrieved the campaign with the specified id
 * @param req
 * @param h
 */
export const getById = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {
    // Query parameters
    const { id } = req.params
    let { fields } = req.query

    // Search Query
    const searchQuery = { "_id": ObjectId(id) }

    // Projection
    const projectQuery = getProjection(fields)

    // Database Query
    const db = await getDatabase(req.server);
    const data = await UserService.getOne(db, searchQuery, projectQuery)

    // Return values
    return h
        .response(data)
        .code(200)
}

/**
 *  Creates a single theme
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const createOne = async (req: any, h: Hapi.ResponseToolkit) => {

    //Retrieve user input and set default values
    let { firstName, lastName, email, password } : Models.DFB.UserCreation = req.payload

    // Sanitize user input
    firstName = sanitize(firstName)
    lastName = sanitize(lastName)
    email = sanitize(email)
    password = sanitize(password)

    // Duplication Check
    const db = await getDatabase(req.server);
    const existing = await AuthService.getOne(db, { "email": email }, { title: 1 })
    if (existing) {
        return h.response("Email already in use.").code(409)
    }

    // Create user
    const metaUserProfile: Models.DFB.Meta = {
        createdAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        createdBy: "-",
        modifiedAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        modifiedBy: "-"
    }
    const user : Models.DFB.User = {
        firstName: firstName,
        lastName: lastName,
        avatar: "https://i.picsum.photos/id/550/200/300.jpg?hmac=XYJd_N4d9B5UYc7zHt9tTuzuQ4ol2byXjSGvqPAsRkI",
        contact: { email: email, phone: "" },
        role: Roles.v1.user,
        meta: metaUserProfile
    }
    const userInserted = await UserService.insertOne(db, user)
    if (!userInserted) {
        return h.response("Internal Server Error").code(503)
    }

    // Create a refresh tokens
    user.scope = [userInserted.role]
    const refreshToken = await createToken({}, userInserted._id.toString(), req.server.app.jwtRefreshTokenSecret,  req.server.app.jwtRefreshTokenTime )
    const accessToken = await createToken(userInserted, user._id.toString(), req.server.app.jwtAccessTokenSecret, req.server.app.jwtAccessTokenTime )

    // Create Auth account
    const metaAuthAccount: Models.DFB.Meta = {
        createdAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        createdBy: "-",
        modifiedAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        modifiedBy: "-"
    }
    const authAccount : Models.DFB.AuthAccount = {
        email: email,
        password: await hashPassword(password, req.server.app.saltRounds),
        user: userInserted._id,
        refreshTokens: [await hashPassword(refreshToken, req.server.app.saltRounds)], // have to hash refresh tokens because they are basically passwords
        meta: metaAuthAccount
    }
    const authAccountInserted = await AuthService.insertOne(db, authAccount)
    if (!authAccountInserted) {
        return h.response("Internal Server Error").code(503)
    }

    // Email Notification
    const emailData = {
        discoverURL: `https://${req.headers.host}/discover`,
        name: firstName? firstName : email.match(/^([^@]*)@/)[1],
        username: email,
        login_url: `https://${req.headers.host}/signIn`,
        support_email: "support@team-stage.com",
        help_url: `https://${req.headers.host}/help`,
        action_url: `https://${req.headers.host}/user`,
    }
    await send("welcome", email, "DFB — Great to see you!", emailData)

    // Return values
    return h.response({
        refresh_token: refreshToken,
        access_token: accessToken,
        user: userInserted
    }).code(201)
}
