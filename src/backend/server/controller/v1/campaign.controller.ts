"use strict"

import * as Hapi from "@hapi/hapi";
import { getMany, getOne, insertOne, updateOne, getUserOne, getStatusOne, getCount } from "../../services/v1/campaign.service";
import { Models } from "../../types/models";
import { getDatabase } from "../../helpers/db.helper";
const ObjectId = require("mongodb").ObjectID
const sanitize = require("mongo-sanitize");
const moment = require('moment');


const DEFAULT_SKIP = 0;
const DEFAULT_LIMIT = 10000;

/**
 *
 * @param fields
 */
const getProjection = (fields) => {
    let fieldsProjection = {}
    if (fields) {
        const fieldsKeyWords = fields.split(',');
        for (const fieldKeyWord of fieldsKeyWords) {
            fieldsProjection[fieldKeyWord] = 1
        }
    } else {
        fieldsProjection = { _id: 1, title: 1, description: 1,image: 1, startDate: 1, endDate: 1, tags: 1, socialMedia: 1, user: 1, status: 1, meta: 1 }
    }

    return fieldsProjection
}

/**
 *
 * @param sort
 */
const getSortQuery = (sort) => {
    let sortQuery = {}
    if (sort) {
        const sortKeyWords = sort.split(',');
        for (const sortKeyWord of sortKeyWords) {
            const index = sortKeyWord.charAt(0) === "-" ? sortKeyWord.substring(1, sortKeyWord.length) : sortKeyWord
            sortQuery[index] = (sortKeyWord.charAt(0) === "-") ? -1 : 1
        }
    } else {
        sortQuery = {
            title: 1
        }
    }

    return sortQuery
}

/**
 *
 * @param search
 */
const getSearchQuery = (search) => {
    const { id, title, status = "Approved"  } = search

    const searchQuery = {
        _id: ObjectId,
        title: String,
        status: String,
    }

    if (id) {
        searchQuery._id = ObjectId(id)
    }
    if (title) {
        searchQuery.title = title
    }
    searchQuery.status = status

    return searchQuery
}

/**
 *
 * @param search
 */
const getSearchQueryPending = (search) => {
    const { id, title, status = "Pending" } = search

    const searchQuery = {
        _id: ObjectId,
        title: String,
        status: String,
    }

    if (id) {
        searchQuery._id = ObjectId(id)
    }
    if (title) {
        searchQuery.title = title
    }
    searchQuery.status = status

    return searchQuery
}

/**
 * Retrieved one or multiple campaigns
 * @param req
 * @param h
 */
export const get = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {

    // User input
    const { limit, skip, fields, sort, ...searchQueryVariables } = req.query;

    // TODO Input sanitization

    // Database Query Construction
    const searchQuery = getSearchQuery(searchQueryVariables);
    const sortQuery = getSortQuery(sort);
    const projectionQuery = getProjection(fields);
    const limitQuery = DEFAULT_LIMIT;
    const skipQuery = DEFAULT_SKIP;

    // Database Connection
    const db = await getDatabase(req.server);

    const data = await getMany(db, searchQuery, limitQuery, skipQuery, sortQuery, projectionQuery);
    const dataCount = await getCount(db, searchQuery);

    // Pagination
    const pagination = { limit: limit, skip: skip, count: Array.isArray(data) ? data.length : 1, totalElements: dataCount }

    // Return values
    return h
        .response({ data, pagination })
        .code(200)
}

/**
 * Retrieved one or multiple campaigns
 * @param req
 * @param h
 */
export const getPending = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {

    // User input
    const { limit, skip, fields, sort, ...searchQueryVariables } = req.query;

    // TODO Input sanitization

    // Database Query Construction
    const searchQuery = getSearchQueryPending(searchQueryVariables);
    const sortQuery = getSortQuery(sort);
    const projectionQuery = getProjection(fields);
    const limitQuery = DEFAULT_LIMIT;
    const skipQuery = DEFAULT_SKIP;

    // Database Connection
    const db = await getDatabase(req.server);

    const data = await getMany(db, searchQuery, limitQuery, skipQuery, sortQuery, projectionQuery);
    const dataCount = await getCount(db, searchQuery);

    // Pagination
    const pagination = { limit: limit, skip: skip, count: Array.isArray(data) ? data.length : 1, totalElements: dataCount }

    // Return values
    return h
        .response({ data, pagination })
        .code(200)
}

/**
 * Retrieved the campaign with the specified id
 * @param req
 * @param h
 */
export const getById = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {
    // Query parameters
    const { id } = req.params
    let { fields } = req.query

    // Search Query
    const searchQuery = { "_id": ObjectId(id) }

    // Projection
    const projectQuery = getProjection(fields)

    // Database Query
    const db = await getDatabase(req.server);
    const data = await getOne(db, searchQuery, projectQuery)

    // Return values
    return h
        .response(data)
        .code(200)
}

/**
 *  Creates a single theme
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const createOne = async (req: any, h: Hapi.ResponseToolkit) => {

    const userObject = req.auth.credentials

    //Retrieve user input and set default values
    let { instagram, facebook, twitter, xing, linkedIn, website }: Models.DFB.SocialMedia = req.payload
    let { title, description, startDate, endDate, tags }: Models.DFB.Campaign = req.payload;

    // Sanitize user input
    title = sanitize(title)
    description = sanitize(description)
    startDate = sanitize(startDate)
    endDate = sanitize(endDate)
    tags = sanitize(tags)
    instagram = sanitize(instagram)
    facebook = sanitize(facebook)
    twitter = sanitize(twitter)
    xing = sanitize(xing)
    linkedIn = sanitize(linkedIn)
    website = sanitize(website)


    // Duplication Check
    const db = await getDatabase(req.server);
    const existingCampaign = await getOne(db, { "title": title }, { title: 1 })
    if (existingCampaign) {
        return h.response("A Campaign with that title already exists").code(409)
    }

    const statusData = await getStatusOne(db, { "name": 'Pending' }, { name: 1 })

    // Add meta
    const meta: Models.DFB.Meta = {
        createdAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        createdBy: userObject.sub,
        modifiedAt: moment().utc().format("DD-MM-YYYY HH:mm"),
        modifiedBy: userObject.sub
    }

    // Insertion
    const campaign: Models.DFB.Campaign = { title: title, description: description, image: "images/placeholder_1.jpg", startDate: startDate, endDate: endDate, tags: tags, socialMedia: { instagram: instagram, facebook: facebook, xing: xing, twitter: twitter, linkedIn: linkedIn, website: website }, user: userObject.sub, status: statusData.name, meta: meta }
    const data = await insertOne(db, campaign)
    if (!data) {
        return h.response("Internal Server Error").code(503)
    }

    // Return values
    return h.response(data).code(201)

}

/**
 *  Modifies a singe campaign
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const partiallyUpdateOneById = async (req: any, h: Hapi.ResponseToolkit) => {
    //Retrieve user input and set default values
    const campaignId = req.params.id
    let { instagram, facebook, twitter, xing, linkedIn, website }: Models.DFB.SocialMedia = req.payload
    let { title, description, startDate, endDate, tags, status }: Models.DFB.Campaign = req.payload;

    // Sanitize user input
    title = sanitize(title)
    description = sanitize(description)
    startDate = sanitize(startDate)
    endDate = sanitize(endDate)
    tags = sanitize(tags)
    status = sanitize(status)
    instagram = sanitize(instagram)
    facebook = sanitize(facebook)
    twitter = sanitize(twitter)
    xing = sanitize(xing)
    linkedIn = sanitize(linkedIn)
    website = sanitize(website)

    // Duplication Check
    const db = await getDatabase(req.server);
    const campaign = await getOne(db, { "_id": ObjectId(campaignId) }, {})

    const existingCampaign = await getOne(db, { "title": title }, { title: 1 })
    if (existingCampaign && !(existingCampaign.title === campaign.title)) {
        return h.response("A Campaign with that title already exists").code(409)
    }

    // Update Object
    if (title) campaign.title = title
    if (description) campaign.description = description
    if (startDate) campaign.startDate = startDate
    if (endDate) campaign.endDate = endDate
    if (tags) campaign.tags = tags
    if (status) campaign.status = status
    if (instagram) campaign.socialMedia.instagram = instagram
    if (facebook) campaign.socialMedia.facebook = facebook
    if (twitter) campaign.socialMedia.twitter = twitter
    if (xing) campaign.socialMedia.xing = xing
    if (linkedIn) campaign.socialMedia.xing = linkedIn
    if (website) campaign.socialMedia.xing = website

    // Insertion
    const data = await updateOne(db, { "_id": ObjectId(campaignId) }, campaign)
    if (!data || !data.value) {
        return h.response("Internal Server Error").code(503)
    }

    // Return values
    return h.response(data.value).code(200)
}