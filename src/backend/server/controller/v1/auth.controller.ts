"use strict"

import * as Hapi from "@hapi/hapi";
import { getDatabase } from "../../helpers/db.helper";
import { createToken, hashPassword } from "../../helpers/authentication.helper";
import * as AuthService from "../../services/v1/auth.service";
import * as UserService from "../../services/v1/user.service";
import {send} from "../../helpers/mailer";
const ObjectId = require("mongodb").ObjectID
const sanitize = require("mongo-sanitize");
const Jwt = require("jsonwebtoken")
const Bcrypt = require("bcrypt")
const moment = require('moment');

const DEFAULT_SKIP = 0;
const DEFAULT_LIMIT = 10000;

/**
 *  Created an access token based on a refresh token
 *
 * At first we check if the token is still valid (expiration, algorithm, ...)
 * After that we check if the person that the refresh token (allegedly) relates to still exists
 * After that we check if the refresh token is actually related to the user
 *
 * If everything went okay we return the access token
 * If anything is wrong we return an error
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const login = async (req: any, h: Hapi.ResponseToolkit) => {
    //Retrieve user input and set default values
    let { email, password } = req.payload

    // Sanitize user input
    email = sanitize(email)
    password = sanitize(password)

    // Check if the user exists
    const db = await getDatabase(req.server);
    let authAccount = await AuthService.getOne(db, { "email": email }, { email: 1, password: 1 })
    if (!authAccount || !authAccount.email || !authAccount.password) {
        return h.response("Incorrect email or password").code(409)
    }
    // Check if the password matches
    if (!await Bcrypt.compare(password, authAccount.password)) {
        return h.response("Incorrect email or password").code(409)
    }
    const user = await UserService.getOne(db, { "_id": ObjectId(authAccount.user) }, { firstName: 1, _id: 1 })
    if (!user) {
        return h.response("Something went wrong").code(500)
    }
    user.scope = [user.role]

    // Create a refresh token
    const refreshToken = await createToken({}, user._id.toString(), req.server.app.jwtRefreshTokenSecret, req.server.app.jwtRefreshTokenTime)
    const accessToken = await createToken(user, user._id.toString(), req.server.app.jwtAccessTokenSecret, req.server.app.jwtAccessTokenTime)

    // TODO optimize the speed of this - combine two queries to one
    authAccount.meta.modifiedAt = moment().utc().format("DD-MM-YYYY HH:mm")
    authAccount.meta.modifiedBy = ObjectId(user._id)
    await AuthService.pushRefreshToken(db, { "email": email }, await hashPassword(refreshToken, req.server.app.saltRounds), authAccount.meta)
    authAccount = await AuthService.getOne(db, { "email": email }, { email: 1, password: 1 })
    if (!authAccount || !authAccount.email || !authAccount.password) {
        // TODO how to handle this edge case?
        return h.response("Something went wrong").code(500)
    }

    return h.response({
        refresh_token: refreshToken,
        access_token: accessToken,
        user: user
    }).code(200)
}

/**
 *  Created an access token based on a refresh token
 *
 * At first we check if the token is still valid (expiration, algorithm, ...)
 * After that we check if the person that the refresh token (allegedly) relates to still exists
 * After that we check if the refresh token is actually related to the user
 *
 * If everything went okay we return the access token
 * If anything is wrong we return an error
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const refresh = async (req: any, h: Hapi.ResponseToolkit) => {

    //Retrieve user input and set default values
    let { refresh_token } = req.payload

    // Sanitize user input
    refresh_token = sanitize(refresh_token)

    // Verify token
    let decoded = null
    try {
        decoded = Jwt.verify(refresh_token, req.server.app.jwtRefreshTokenSecret);
    } catch (err) {
        req.logger.info(err)
    }
    if (!decoded) {
        return h.response("Refresh token is invalid").code(409)
    }

    // Check against impersonation
    // 1) User profile does not exist or is blocked
    const db = await getDatabase(req.server);
    const user = await UserService.getOne(db, { "_id": ObjectId(decoded.sub) }, { email: 1 })
    if (!user) {
        return h.response("Refresh token is invalid").code(409)
    }
    // 2) Auth account does not exist or is blocked
    const authAccount = await AuthService.getOne(db, { "user": ObjectId(decoded.sub) }, { email: 1 })
    if (!authAccount) {
        return h.response("Refresh token is invalid").code(409)
    }
    // 3) Refresh token is not related to the user
    const isTokenRelatedToUser = authAccount.refreshTokens.filter(token => {
        return Bcrypt.compare(refresh, token);
    })
    if (!isTokenRelatedToUser) {
        return h.response("Refresh token is invalid").code(409)
    }

    // Create an access token
    user.scope = [user.role]
    const accessToken = await createToken(user, decoded.sub, req.server.app.jwtAccessTokenSecret, req.server.app.jwtAccessTokenTime)

    return h.response({ access_token: accessToken }).code(200)
}

/**
 * Retrieved the user from the access token
 * @param req
 * @param h
 */
export const getUserFromAuthorizationToken = async (req: Hapi.Request, h: Hapi.ResponseToolkit) => {
    // Search Query
    const id = req.auth.credentials.sub

    // Database Query
    const db = await getDatabase(req.server);
    const data = await UserService.getOne(db, {"_id": ObjectId(id) }, {})

    if (!data) {
        return h.response("Could not find a user with that id").code(404)
    }

    // Return values
    return h
        .response({ user: data })
        .code(200)
}

/**
 *  Send a password reset email
 *
 * Supported query parameters: None
 *
 * @param req Request object
 * @param h Handler of the response
 */
export const passwordReset = async (req: any, h: Hapi.ResponseToolkit) => {
    //Retrieve user input and set default values
    let { email } = req.payload
    email = sanitize(email)

    // Email template
    const emailData = {
        discoverURL: `https://${req.headers.host}/discover`,
        name: "",
        username: email,
        ip: req.info.remoteAddress,
        browser_name: req.info.remoteAddress,
        support_url: "support@team-stage.com",
        action_url: `https://${req.server.app.baseUrl}/forgotPassword`,
        email_address: email
    }

    // Check if the user exists
    const db = await getDatabase(req.server);
    let authAccount = await AuthService.getOne(db, { "email": email }, { email: 1, password: 1 })
    if (!authAccount || !authAccount.email || !authAccount.password) {
        await send("password-reset-help", email, "DFB — Password Reset!", emailData)
        return h.response("We have send you an email").code(200)
    }

    // Get user profile information for email personalization
    const user = await UserService.getOne(db, { "_id": ObjectId(authAccount.user) }, { firstName: 1, _id: 1 })
    // if (!user) {
    //     return h.response("Something went wrong").code(500)
    // }
    // user.scope = [user.role]
    //
    // // Create a refresh token
    // const refreshToken = await createToken({}, user._id.toString(), req.server.app.jwtRefreshTokenSecret, req.server.app.jwtRefreshTokenTime)
    // const accessToken = await createToken(user, user._id.toString(), req.server.app.jwtAccessTokenSecret, req.server.app.jwtAccessTokenTime)

    // Email Notification
    // @ts-ignore
    emailData.name = user.firstName
    await send("password-reset", email, "DFB — Password Reset!", emailData)

    return h.response("We have send you an email").code(200)
}