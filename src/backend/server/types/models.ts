
export namespace Models {
    export namespace DFB {
        export interface Meta {
            createdAt: String,
            createdBy: String,
            modifiedAt: String,
            modifiedBy: String
        }

        export interface Campaign {
            title: String,
            description: String,
            image: String,
            startDate: Date,
            endDate: Date,
            tags: Array<String>,
            socialMedia: SocialMedia,
            status: String,
            user: User,
            meta: Meta
        }

        export interface SocialMedia {
            instagram?: String,
            facebook?: String,
            twitter?: String,
            xing?: String,
            linkedIn?: String,
            website?: String,
        }

        export interface User {
            _id?: String,
            firstName: String,
            lastName: String,
            contact: Contact,
            avatar: String,
            role: String,
            scope?: Array<String>,
            meta: Meta
        }

        export interface AuthAccount {
            email: String,
            password: String,
            user: String,
            refreshTokens: Array<String>,
            meta: Meta
        }

        export interface UserCreation {
            firstName: String,
            lastName: String,
            email: String,
            password: String
        }

        export interface Contact {
            email: String,
            phone: String
        }

        export interface Role {
            name: String,
            _id: String
        }
    }
}