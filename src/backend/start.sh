#!/bin/bash

if [ "$NODE_ENV" == "develop" ]; then
    npm run dev
else
    npm run start
fi
