apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress
spec:
  rules:
    - host: $CI_ENVIRONMENT_DOMAIN
      http:
        paths:
          - path: /
            backend:
              serviceName: timetable-frontend
              servicePort: 3000